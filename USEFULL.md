# Release from dev to master

## push to dev with merge request to master

git push -o merge_request.create -o merge_request.target=master -o merge_request.merge_when_pipeline_succeeds -o merge_request.title="pages helm repository"

## create and merge a merge request from dev to master
export gl_private_token=$(cat ~/.gitlab-token)

MR_ID=$(curl --header "PRIVATE-TOKEN: $gl_private_token" --header 'Content-Type: application/json'  -L -X POST https://gitlab.com/api/v4/projects/32404365/merge_requests -d '{"source_branch": "dev", "target_branch": "master", "title": "Release v0.1.4" }' | jq -r '.iid')

curl --header "PRIVATE-TOKEN: $gl_private_token" --header 'Content-Type: application/json'  -L -X PUT https://gitlab.com/api/v4/projects/32404365/merge_requests/$MR_ID/merge -d '{"merge_when_pipeline_succeeds": true}'
