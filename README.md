# cluster-api-vsphere

Helm chart to create kubernetes cluster using [cluster-api](https://github.com/kubernetes-sigs/cluster-api) and its [vsphere provider](https://github.com/kubernetes-sigs/cluster-api-provider-vsphere)

![Version: 4.1.5](https://img.shields.io/badge/Version-4.1.5-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.2.0](https://img.shields.io/badge/AppVersion-1.2.0-informational?style=flat-square)

## TL;DR

```shell
$ helm repo add cluster-api-vsphere https://labo-pe.gitlab.io/capv-helm-chart/
$ helm show values cluster-api-vsphere/cluster-api-vsphere
```

## Issues and contributions

Source code and issues for this helm chart are hosted on gitlab :
  * [source code](https://gitlab.com/labo-pe/capv-helm-chart)
  * [issues](https://gitlab.com/labo-pe/capv-helm-chart/-/issues)

## Prerequisites

  * cluster-api 1.1+
  * cluster-api-provider-vsphere 1.2+

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| commonLabels | object | `{}` | Labels to add to all objects generated by this chart |
| commonAnnotations | object | `{}` | Annotations to add to all objects generated by this chart |
| wait.cluster | bool | `false` | Deploy a job waiting for machines to rollout after deployment |
| wait.apiServer | bool | `false` | Deploy a job waiting for apiServer to be available |
| wait.timeout | int | `900` | max time waiting for resources to rollout |
| cluster.name | string | `""` | Cluster name. If unset, the release name will be used |
| cluster.controlPlaneEndpoint.host | string | `""` | IP or DNS name of the kubernetes endpoint |
| cluster.controlPlaneEndpoint.port | int | `6443` | Kubernetes endpoint port |
| cluster.controlPlaneEndpoint.interface | string | `""` | Linux interface on the node |
| cluster.podCidrBlocks | list | `["19.0.0.0/16"]` | Network CIDR for pods |
| cluster.servicesCidrBlocks | list | `["56.0.0.0/16"]` | Network CIDR for services |
| cluster.additionalClusterResourceSet | list | `[]` | Additionnal ClusterResource to add to ClusterResourceSet |
| kubernetes.version | string | `"v1.24.7"` | Version of kubernetes |
| kubernetes.additionnalClientCaFile | string | `""` | Pem encoded certificate to authenticate clients over x509 |
| kubernetes.kubeadm.commonKubeletExtraArgs | object | `{}` | Additional kubelet command line arguments for init and join configurations |
| kubernetes.kubeadm.controlPlane.clusterConfiguration | object | `{}` | Kubeadm cluster configuration, more info : https://kubernetes.io/docs/reference/config-api/kubeadm-config.v1beta3/ |
| kubernetes.kubeadm.controlPlane.additionalFiles | list | `[]` | Additional files to create on the kubeadm control plane instances |
| kubernetes.kubeadm.controlPlane.preKubeadmAdditionalCommands | list | `[]` | Additional commands to execute on control plane before kubeadm join/init |
| kubernetes.kubeadm.controlPlane.postKubeadmAdditionalCommands | list | `[]` | Additional commands to execute on control plane after kubeadm join/init |
| kubernetes.kubeadm.controlPlane.patchesDirectory | string | `""` | Directory containing kubeadm patches on target vms |
| kubernetes.kubeadm.workers.files | list | `[]` | files to create on the workers instances |
| kubernetes.kubeadm.workers.preKubeadmAdditionalCommands | list | `[]` | Additional commands to execute on workers before kubeadm join/init |
| kubernetes.kubeadm.workers.postKubeadmAdditionalCommands | list | `[]` | Additional commands to execute on workers after kubeadm join/init |
| machines.dhcp4 | bool | `true` | Use dhcp for ipv4 configuration |
| machines.addressesFromPools | object | `{"enabled":false,"providers":[]}` | Use an IPAMProvider pool to reserve IPs |
| machines.addressesFromPools.enabled | bool | `false` | Enable the IPAMProvider usage |
| machines.addressesFromPools.providers | list | `[]` | List of providers to reserve IPs from |
| machines.nameServers | list | `[]` | Nameservers for VMs DNS resolution |
| machines.searchDomains | list | `[]` | Search domains suffixes to configure on VMs |
| machines.domain | string | `""` | VM network domain |
| machines.gateway | string | `""` | IPv4 gateway |
| machines.users | list | `[]` | users to create on machines see https://github.com/kubernetes-sigs/cluster-api/blob/main/bootstrap/kubeadm/api/v1beta1/kubeadmconfig_types.go#L257 for documentation about user config object |
| machines.controlPlane.annotations | object | `{}` | Control plane VsphereMachineTemplate annotations |
| machines.controlPlane.replicas | int | `3` | Number of control plane VMs instances |
| machines.controlPlane.kubeVipVersion | string | `"v0.5.5"` | kube-vip version |
| machines.controlPlane.ipAddrs | list | `[]` | ipaddrs passed to capi if necessary |
| machines.controlPlane.criSocket | string | `"/var/run/containerd/containerd.sock"` | Path of the CRI socket to use |
| machines.controlPlane.resourcePool | string | `""` | Control plane VSphere resource pool |
| machines.controlPlane.template | string | `""` | Control plane VSphere machine template to clone, must contain kubeadm at the same version as specified in kubernetes.version |
| machines.controlPlane.folder | string | `""` | Control plane VSphere folder to store VM |
| machines.controlPlane.storagePolicy | string | `""` | Control plane VSphere storage policy name to use for disks |
| machines.controlPlane.dataStore | string | `""` | Control plane VSphere datastore to create/locate machine |
| machines.controlPlane.cpuCount | int | `2` | Control plane Number of vCPUs to allocate to controlPlane instances |
| machines.controlPlane.diskSizeGiB | int | `40` | Control plane Disk size of VM in GiB |
| machines.controlPlane.memorySizeMiB | int | `4096` | Control plane Memory to allocate to controlPlane VMs |
| machines.controlPlane.kubeletExtraArgs | object | `{}` | Additional kubelet command line arguments for join configurations |
| machines.controlPlane.nodeDrainTimeout | string | `""` | Node drain timeout is the total amount of time that the controller will spend on draining a node |
| machines.controlPlane.machineHealthCheck.enabled | bool | `false` | Deploys a machineHealthCheck object for the controlPlane |
| machines.controlPlane.machineHealthCheck.maxUnhealthy | string | `"100%"` | Any further remediation is only allowed if at most "MaxUnhealthy" machines are not healthy |
| machines.controlPlane.machineHealthCheck.nodeStartupTimeout | string | `""` | Machines older than this duration without a node will be considered to have failed and will be remediated |
| machines.controlPlane.machineHealthCheck.unhealthyConditions | list | `[{"status":"Unknown","timeout":"300s","type":"Ready"},{"status":"False","timeout":"300s","type":"Ready"}]` | list of the conditions that determine whether a node is considered unhealthy. if any of the conditions is met, the node is unhealthy. |
| machines.controlPlane.machineHealthCheck.unhealthyRange | string | `""` | remediation is only allowed if the number of machines selected by "selector" as not healthy is within the range of "UnhealthyRange" |
| machines.workers | object | `{}` | Worker pools, more details on its configuration in [Worker pools configuration](#worker-pools-configuration) |
| vsphere.dataCenter | string | `""` | Datacenter to use |
| vsphere.network | string | `""` | VSphere network for VMs and CSI |
| vsphere.username | string | `""` | Vsphere username |
| vsphere.password | string | `""` | VSphere password |
| vsphere.server | string | `""` | VSphere server dns name |
| vsphere.tlsThumbprint | string | `""` | VSphere https TLS thumbprint |
| vsphereCsi.enabled | bool | `true` | Installs vsphere-csi on the cluster |
| vsphereCsi.cloudControllerManager.resources | object | `{"requests":{"cpu":"200m"}}` | resources of vsphere-cloud-controller-manager |
| vsphereCsi.controller.csiAttacher.resources | object | `{}` | resources of the container csi-attacher in vsphere-csi-controller |
| vsphereCsi.controller.csiResizer.resources | object | `{}` | resources of the container csi-resizer in vsphere-csi-controller |
| vsphereCsi.controller.vsphereCsiController.resources | object | `{}` | resources of the container vsphere-csi-controller in vsphere-csi-controller |
| vsphereCsi.controller.vsphereSyncer.resources | object | `{}` | resources of the container vsphere-syncer in vsphere-csi-controller |
| vsphereCsi.controller.csiProvisioner.resources | object | `{}` | resources of the container csi-provisioner in vsphere-csi-controller |
| vsphereCsi.controller.csiSnapshotter.resources | object | `{}` | resources of the container csi-snapshotter in vsphere-csi-controller |
| vsphereCsi.controller.livenessProbe.resources | object | `{}` | resources of the container liveness-probe in vsphere-csi-controller |
| vsphereCsi.node.nodeDriverRegistrar.resources | object | `{}` | resources of the container node-driver-registrar in vsphere-csi-node |
| vsphereCsi.node.vsphereCsiNode.resources | object | `{}` | resources of the container vsphere-csi-node in vsphere-csi-node |
| vsphereCsi.node.livenessProbe.resources | object | `{}` | resources of the container liveness-probe in vsphere-csi-node |
| storageClass.enabled | bool | `false` | Create storage class |
| storageClass.name | string | `"default"` | Storage class name |
| storageClass.default | bool | `true` | Define storage class as default class |
| storageClass.reclaimPolicy | string | `"Delete"` | Storage class reclaimPolicy |
| storageClass.fsType | string | `"ext4"` | Storage class fsType |
| storageClass.storagePolicy | string | `""` | VSphere Storage policy |
| cni.calico.enabled | bool | `false` | Installs cni calico on the cluster |
| cni.calico.config | string | `""` | specific config for calico, see https://projectcalico.docs.tigera.io/getting-started/kubernetes/installation/config-options |

## About this chart

### Application version

The application version of this chart reflect the supported version of the [cluster api provider vsphere](https://github.com/kubernetes-sigs/cluster-api-provider-vsphere)

## Configuration and installation details

### cluster upgrade

This chart creates a VSphereMachineTemplate for the control-plane and another one for the workers. Each of those are named with a sha256 identifier which will change if their content is modified.

During a `helm upgrade`, the new `VSphereMachineTemplates` will be created, the `KubeadmControlPlane` and `MachineDeployment` will be updated with the new names and trigger a RollingUpdate.

In other words :
  * Change the values
  * Launch an upgrade
  * Wait and see your cluster being updated

__note:__ The control-plane old template is removed at deployment, but not the deployment templates, as, after upgrade, the old `MachineSets` still references the them

### Worker pools configuration

You can configure as many worker pools as desired, the value `machines.workers` takes a map in which each key is the worker pool name :

__exemple__:
```yaml
# -- Name of the worker pool, you can define as many others pools as desired
worker-md-0:
  # -- Number of worker VMs instances
  replicas: 1
  # -- MachineDeployment annotations
  machineDeploymentAnnotations: {}
  # -- Labels to add to the machines created by the machineDeployment
  machinesLabels: {}
  # -- Labels to add to the machineDeployment selector to match the machines
  machinesSelectors: {}
  # -- workers VsphereMachineTemplate annotations
  annotations: {}
  # -- ipaddrs passed to capi if necessary
  ipAddrs: []
  # -- Path of the CRI socket to use
  criSocket: "/var/run/containerd/containerd.sock"
  # -- workers VSphere resource pool
  resourcePool: ""
  # -- workers VSphere machine template to clone, must contain kubeadm at the same version as specified in kubernetes.version
  template: ""
  # -- workers VSphere folder to store VM
  folder: ""
  # -- workers VSphere storage policy name to use for disks
  storagePolicy: ""
  # -- workers VSphere datastore to create/locate machine
  dataStore: ""
  # -- Number of vCPUs to allocate to worker instances
  cpuCount: 4
  # -- disk size of workers VM in GiB
  diskSizeGiB: 100
  # -- Memory to allocate to worker VMs
  memorySizeMiB: 8192
  # -- files to create on the workers instances of this pool
  files: []
  # -- Additional commands to execute on workers before kubeadm join/init on this pool
  preKubeadmAdditionalCommands: []
  # -- Additional commands to execute on workers after kubeadm join/init on this pool
  postKubeadmAdditionalCommands: []
  # -- Additional kubelet command line arguments for join configurations
  kubeletExtraArgs: {}
  # -- Node drain timeout is the total amount of time that the controller will spend on draining a node
  nodeDrainTimeout: ""
  machineHealthCheck:
    # -- Deploys a machineHealthCheck object for the workerPool
    enabled: false
    # -- Any further remediation is only allowed if at most "MaxUnhealthy" machines are not healthy
    maxUnhealthy: ""
    # -- Machines older than this duration without a node will be considered to have failed and will be remediated
    nodeStartupTimeout: ""
    # -- list of the conditions that determine whether a node is considered unhealthy. if any of the conditions is met, the node is unhealthy.
    unhealthyConditions:
    - type: Ready
      status: Unknown
      timeout: 300s
    - type: Ready
      status: "False"
      timeout: 300s
    # -- remediation is only allowed if the number of machines selected by "selector" as not healthy is within the range of "UnhealthyRange"
    unhealthyRange: ""
```

### Helm --atomic

Helm waits and check status of deployments and statefulsets, for other kinds of objects, it relies only on the creation and considers it fully deployed if the creation is successful. To address this, the chart contains a Job, deployed as a helm hook, which waits until the control-plane and workers have finished their install/upgrade.

As deploying a full cluster may take a while, the wait hook is disabled by default. It can be enabled by setting `wait.cluster` to `true` and the timeout is configurable through `wait.timeout`.
Since helm timeout by default is 5m, you should also set the helm command line option `--timeout` to something a bit larger than the configured `wait.timeout`

With all this parameters in place, you should be able to use the `--atomic` helm flag

### Configure ipvs on the target cluster

Cluster-api doesn't yet support kubeproxy configuration.
As a workaround you could use the value `kubernetes.kubeadm.controlPlane.additionalFiles` to create a script on the control-plane instances :

```yaml
kubernetes:
  kubeadm:
    controlPlane:
      additionalFiles:
      - path: /tmp/generate-kube-proxy.sh
        permissions: "0700"
        owner: root:root
        content: |
          #!/bin/bash
          for i in $(ls /run/kubeadm/ | grep kubeadm); do
              cat <<EOF>> /run/kubeadm/$i
          ---
          kind: KubeProxyConfiguration
          apiVersion: kubeproxy.config.k8s.io/v1alpha1
          mode: ipvs
          ipvs:
            syncPeriod: 30s
            minSyncPeriod: 2s
            scheduler: rr
          EOF
          done
```
and `kubernetes.kubeadm.controlPlane.preKubeadmAdditionalCommands` to execute the script and add the configuration to the kubeadm.conf file :

```yaml
kubernetes:
  kubeadm:
    controlPlane:
      preKubeadmAdditionalCommands:
      - /tmp/generate-kube-proxy.sh
```

### Client x509 authentication

To configure client x509 authentication, you need to specify `kubernetes.additionnalClientCaFile` to the client ca as a PEM encoded certificate :

```yaml
kubernetes:
  additionnalClientCaFile: |
    -----BEGIN CERTIFICATE-----
    [...]
    -----END CERTIFICATE-----
```

### Additional resources on the new cluster

You can leverage the ClusterAPI `clusterResourceSet` capability through this chart to deploy additionnals resources on the newly created cluster.
To do so, create either a Secret or a ConfigMap in the namespace which will host the cluster resources.

Then, in the values.yaml, specify which resources should be deployed on the cluster:

```yaml
cluster:
  additionalClusterResourceSet:
    - kind: ConfigMap
      name: your_configmap
```

This is how calico-cni is installed and you could use this feature to deploy another cni of your choice or any others resources.
