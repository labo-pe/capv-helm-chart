CAPI_VERSION=${1-v1.1.1}
CAPV_VERSION=${2-v1.0.2}

# CAPI CRDs
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/addons.cluster.x-k8s.io_clusterresourcesetbindings.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/addons.cluster.x-k8s.io_clusterresourcesets.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/cluster.x-k8s.io_clusterclasses.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/cluster.x-k8s.io_clusters.yaml 
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/cluster.x-k8s.io_machinedeployments.yaml 
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/cluster.x-k8s.io_machinehealthchecks.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/cluster.x-k8s.io_machinepools.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/cluster.x-k8s.io_machines.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/config/crd/bases/cluster.x-k8s.io_machinesets.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/controlplane/kubeadm/config/crd/bases/controlplane.cluster.x-k8s.io_kubeadmcontrolplanes.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/controlplane/kubeadm/config/crd/bases/controlplane.cluster.x-k8s.io_kubeadmcontrolplanetemplates.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/bootstrap/kubeadm/config/crd/bases/bootstrap.cluster.x-k8s.io_kubeadmconfigs.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api/${CAPI_VERSION}/bootstrap/kubeadm/config/crd/bases/bootstrap.cluster.x-k8s.io_kubeadmconfigtemplates.yaml

#CAPV CRDs
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_haproxyloadbalancers.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_vsphereclusteridentities.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_vsphereclusters.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_vsphereclustertemplates.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_vspheredeploymentzones.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_vspherefailuredomains.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_vspheremachines.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_vspheremachinetemplates.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/cluster-api-provider-vsphere/${CAPV_VERSION}/config/crd/bases/infrastructure.cluster.x-k8s.io_vspherevms.yaml

helm upgrade --install --namespace cluster-test --create-namespace \
    --set cluster.controlPlaneEndpoint.host="test" \
    --set machines.controlPlane.template="template" \
    --set machines.workers.worker-md-0.template="template" \
    --set vsphere.network="network" \
    --set vsphere.username="test" \
    --set vsphere.password="test" \
    cluster-test chart/cluster-api-vsphere